# tdd-example

### TDD example project.

Как начать работу:

0. Чтобы попробовать работу с проектом, сделайте себе форк репозитория.

![](https://docs.gitlab.com/ee/user/project/repository/img/forking_workflow_fork_button.png)

0.1 Если хотите сделать отдельный проект, на основе этого -- скачайте архив с файлами и добавьте в свой пустой репозиторий.

![](https://i.stack.imgur.com/OtFQq.png)

1. Если вы хотите работать с проектом в PyCharm

- Клонировать репозиторий на машину
```
git clone https://gitlab.com/volodink/tdd-example
```
- Перейти в папку с проектом
```
cd tdd-example
```
- Открыть проект в PyCharm**

Для Windows:
```
pycharm.cmd .
```
Для Linux:
```
pycharm .
```

---

2.  Если вы хотите работать с проектом из командной строки

- Клонировать репозиторий на машину
```
git clone https://gitlab.com/volodink/tdd-example
```
- Перейти в папку с проектом
```
cd tdd-example
```
- Создать виртуальное окружение*
```
python -m venv .venv
```
- Активировать виртуальное окружение
```
source .venv/bin/activate
```
- Установить зависимости 
```
pip install -r requirements.txt
```
- Установить локальные пакеты в режиме редактирования
```
pip install -e .
```
- Запустить тестирование
```
pytest
```

---

\* Убедитесь, что используемый интерпретатор Python версии не ниже 3.7.х

\*\* Если сгенерированы и доступны в системном пути PATH скрипты запуска из командной строки продуктов JetBrains.

![](https://blog.jetbrains.com/wp-content/uploads/2018/08/shell_scripts_original.png)

Подробнее по ссылке: [Toolbox App 1.11: What’s New.](https://blog.jetbrains.com/blog/2018/08/23/toolbox-app-1-11-whats-new/)

---

### Добавление скриптов и программ в PATH.

Для Windows:

[Add to the PATH on Windows 10](https://www.architectryan.com/2018/03/17/add-to-the-path-on-windows-10/)

Для Linux:
1. Добавьте в конец файла ```.bashrc``` строку, указав путь до папки, где лежат сгенерированные скрипты запуска.
```
export PATH="<path-to-pycharm-shell-scripts-directory>:$PATH"
```
2. Перезагрузите конфигурацию терминала
```
source ~/.bashrc
```
